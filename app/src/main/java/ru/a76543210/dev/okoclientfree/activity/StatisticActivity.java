package ru.a76543210.dev.okoclientfree.activity;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.view.PieChartView;
import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.adapter.DailyVotesResultsAdapter;
import ru.a76543210.dev.okoclientfree.model.Vote;
import ru.a76543210.dev.okoclientfree.presenter.VoteStatisticPresenter;

public class StatisticActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{
    @BindView(R.id.vote_statistic_chart) PieChartView vote_statistic_chart;
    @BindView(R.id.date_from_layout) LinearLayout date_from_layout;
    @BindView(R.id.date_to_layout) LinearLayout date_to_layout;
    @BindView(R.id.date_from_text_view) TextView date_from_text_view;
    @BindView(R.id.date_to_text_view) TextView date_to_text_view;
    @BindView(R.id.votes_result_recycler_view) RecyclerView votes_result_recycler_view;
    @BindView(R.id.vote_1_image_view) ImageView vote_1_image_view;
    @BindView(R.id.vote_1_text_view) TextView vote_1_text_view;
    @BindView(R.id.vote_2_image_view) ImageView vote_2_image_view;
    @BindView(R.id.vote_2_text_view) TextView vote_2_text_view;
    @BindView(R.id.vote_3_image_view) ImageView vote_3_image_view;
    @BindView(R.id.vote_3_text_view) TextView vote_3_text_view;
    @BindView(R.id.vote_4_image_view) ImageView vote_4_image_view;
    @BindView(R.id.vote_4_text_view) TextView vote_4_text_view;
    @BindView(R.id.vote_5_image_view) ImageView vote_5_image_view;
    @BindView(R.id.vote_5_text_view) TextView vote_5_text_view;

    @Nullable
    @BindView(R.id.save_statistic_fab) FloatingActionButton save_statistic_fab;

    VoteStatisticPresenter voteStatisticPresenter;
    private DailyVotesResultsAdapter dailyVotesResultsAdapter;

    private String dateFrom;
    private String dateTo;
    private String selectedDate;

    List<Vote> votes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        ButterKnife.bind(this);
        voteStatisticPresenter = VoteStatisticPresenter.getInstance(this);

        generateDates();
        votes = voteStatisticPresenter.getVotesByDate(dateFrom, dateTo);
        initViews(votes);
    }

    private void generateDates() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        dateTo = dateFormat.format(calendar.getTime());
        calendar.add(Calendar.DATE, -31);
        dateFrom = dateFormat.format(calendar.getTime());
    }

    private void fillDateFields() {
        date_from_text_view.setText(dateFrom);
        date_to_text_view.setText(dateTo);
    }

    private void initViews(List<Vote> votes) {
        fillDateFields();
        fillPieChart(votes);
        fillSummary(votes);
        fillRecyclerView(votes);
        initDateSelectViews();

        save_statistic_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });
    }

    private void fillSummary(List<Vote> votes) {
        Map<Integer, Integer> statistic = voteStatisticPresenter.getVotesStatistic(votes);
        for(Map.Entry<Integer, Integer> voteSum : statistic.entrySet()){
            switch (voteSum.getKey()){
                case 1:
                    vote_1_image_view.setVisibility(View.VISIBLE);
                    vote_1_text_view.setVisibility(View.VISIBLE);
                    vote_1_text_view.setText(voteSum.getValue().toString());
                    break;
                case 2:
                    vote_2_image_view.setVisibility(View.VISIBLE);
                    vote_2_text_view.setVisibility(View.VISIBLE);
                    vote_2_text_view.setText(voteSum.getValue().toString());
                    break;
                case 3:
                    vote_3_image_view.setVisibility(View.VISIBLE);
                    vote_3_text_view.setVisibility(View.VISIBLE);
                    vote_3_text_view.setText(voteSum.getValue().toString());
                    break;
                case 4:
                    vote_4_image_view.setVisibility(View.VISIBLE);
                    vote_4_text_view.setVisibility(View.VISIBLE);
                    vote_4_text_view.setText(voteSum.getValue().toString());
                    break;
                case 5:
                    vote_5_image_view.setVisibility(View.VISIBLE);
                    vote_5_text_view.setVisibility(View.VISIBLE);
                    vote_5_text_view.setText(voteSum.getValue().toString());
                    break;
            }
        }
    }

    private void initDateSelectViews() {


        date_from_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = "from";
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Date date = null;
                try {
                    date = sdf.parse(dateFrom);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendarForDatePickerDialog = Calendar.getInstance();
                calendarForDatePickerDialog.setTime(date);
                new DatePickerDialog(StatisticActivity.this, StatisticActivity.this,
                        calendarForDatePickerDialog.get(Calendar.YEAR),
                        calendarForDatePickerDialog.get(Calendar.MONTH),
                        calendarForDatePickerDialog.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });


        date_to_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = "to";
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Date date = null;
                try {
                    date = sdf.parse(dateTo);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendarForDatePickerDialog = Calendar.getInstance();
                calendarForDatePickerDialog.setTime(date);
                new DatePickerDialog(StatisticActivity.this, StatisticActivity.this,
                        calendarForDatePickerDialog.get(Calendar.YEAR),
                        calendarForDatePickerDialog.get(Calendar.MONTH),
                        calendarForDatePickerDialog.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });
    }


    private void fillPieChart(List<Vote> votes) {
        PieChartData pieChartData = new PieChartData(voteStatisticPresenter.getVotesStatisticValues(votes));
        pieChartData.setHasCenterCircle(true);
        pieChartData.setHasLabels(true);
        vote_statistic_chart.setPieChartData(pieChartData);
    }

    private void fillRecyclerView(List<Vote> votes){
        Map<String, Map<Integer, Integer>> votesDayByDayStatistic = voteStatisticPresenter.getVotesDayByDayStatistic(votes);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        votes_result_recycler_view.setLayoutManager(layoutManager);

        dailyVotesResultsAdapter = new DailyVotesResultsAdapter(votesDayByDayStatistic, this);
        votes_result_recycler_view.setAdapter(dailyVotesResultsAdapter);
    }

    private void saveImage() {
        View save_statistic_layout = getLayoutInflater().inflate(R.layout.save_statistic_layout, null);
        ButterKnife.bind(this, save_statistic_layout);
        fillDateFields();
        fillPieChart(votes);
        fillSummary(votes);
        fillRecyclerView(votes);

        save_statistic_layout.setDrawingCacheEnabled(true);

        save_statistic_layout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        save_statistic_layout.layout(0, 0, save_statistic_layout.getMeasuredWidth(), save_statistic_layout.getMeasuredHeight());

        save_statistic_layout.buildDrawingCache(true);
        Bitmap b = save_statistic_layout.getDrawingCache();
        try {
            File file = new File(getApplicationContext().getFilesDir(), "image.jpg");
            System.out.println(file);
            b.compress(Bitmap.CompressFormat.JPEG, 95, new FileOutputStream(file));
            MediaStore.Images.Media.insertImage(getContentResolver(), b, "" , "");
            Toast.makeText(getApplicationContext(),getString(R.string.statistic_image_saved), Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(),"ERROR!"+e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
        save_statistic_layout.setDrawingCacheEnabled(false);
        ButterKnife.bind(this);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendarForDatePickerDialog = Calendar.getInstance();
        calendarForDatePickerDialog.set(Calendar.YEAR, year);
        calendarForDatePickerDialog.set(Calendar.MONTH, month);
        calendarForDatePickerDialog.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        if(selectedDate.equals("from")){
            dateFrom = dateFormat.format(calendarForDatePickerDialog.getTime());
            date_from_text_view.setText(dateFrom);
        }
        if(selectedDate.equals("to")){
            dateTo = dateFormat.format(calendarForDatePickerDialog.getTime());
            date_to_text_view.setText(dateTo);
        }



        votes = voteStatisticPresenter.getVotesByDate(dateFrom, dateTo);
        initViews(votes);
    }
}
