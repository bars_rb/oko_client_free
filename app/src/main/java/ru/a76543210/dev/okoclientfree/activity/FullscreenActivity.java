package ru.a76543210.dev.okoclientfree.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.a76543210.dev.okoclientfree.OnSwipeTouchListener;
import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.dialog.AdministrationDialog;
import ru.a76543210.dev.okoclientfree.fragment.FiveButtonsFragment;
import ru.a76543210.dev.okoclientfree.fragment.FragmentFactory;
import ru.a76543210.dev.okoclientfree.fragment.ThreeButtonsFragment;
import ru.a76543210.dev.okoclientfree.fragment.TwoButtonsFragment;

public class FullscreenActivity extends AppCompatActivity {
    @BindView(R.id.menu_caller) View menu_caller;


    @BindView(R.id.hint_layout) View hint_layout;
    @BindView(R.id.menu_hint) View menu_hint;



    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    Fragment voteFragment;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        sharedPreferences = getSharedPreferences("Settings", MODE_PRIVATE);

        menu_caller.setOnTouchListener(new OnSwipeTouchListener(FullscreenActivity.this) {
            @Override
            public void onSwipeBottom() {
                AdministrationDialog addExerciseSetResultDialog = new AdministrationDialog();
                addExerciseSetResultDialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
            }
        });
    }

    private void showWarningDialog() {
        hint_layout.setVisibility(View.VISIBLE);

        menu_hint.setOnTouchListener(new OnSwipeTouchListener(FullscreenActivity.this) {
            @Override
            public void onSwipeBottom() {
                AlertDialog alertDialog = new AlertDialog.Builder(FullscreenActivity.this).create();
                alertDialog.setTitle("Warning!");
                alertDialog.setMessage("Pin does not exists! \n" +
                        "Looks like first run.\n" +
                        "Set new pin in settings window!");
                alertDialog.
                        setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                                        startActivity(settingsIntent);
                                    }
                                });
                alertDialog.setCancelable(false);
                alertDialog.show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        showWarningDialog();
        if(sharedPreferences.contains("PIN")) {
            showVoteFragment();
        }
        else{
            showWarningDialog();
        }
    }

    private void showVoteFragment() {
        hint_layout.setVisibility(View.GONE);

        fragmentTransaction = fragmentManager.beginTransaction();

        for (Fragment fragment:getSupportFragmentManager().getFragments()) {
            fragmentTransaction.remove(fragment);
        }

        String voteView = sharedPreferences.getString("VOTE_VIEW", FragmentFactory.FRAGMENTS.get(0));
        voteFragment = FragmentFactory.getFragmentByName(voteView);

        fragmentTransaction.add(R.id.fragment_container, voteFragment, TwoButtonsFragment.FRAGMENT_TAG);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed()
    {

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        hide();
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }
}
