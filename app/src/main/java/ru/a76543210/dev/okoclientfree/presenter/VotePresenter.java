package ru.a76543210.dev.okoclientfree.presenter;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import ru.a76543210.dev.okoclientfree.database.DBProvider;
import ru.a76543210.dev.okoclientfree.database.dao.VoteDao;
import ru.a76543210.dev.okoclientfree.model.Vote;
import ru.a76543210.dev.okoclientfree.service.NetworkWorker;

public class VotePresenter {
    private VotePresenterListener votePresenterListener;
    private static VotePresenter presenterInstance;
    private VoteDao voteDao;

    public static synchronized VotePresenter getInstance(Context context){
        if (presenterInstance == null){
            presenterInstance = new VotePresenter(context);
        }
        return presenterInstance;
    }

    private VotePresenter(Context context){
        voteDao = DBProvider.getAppDatabase(context).voteDao();
        if(voteDao.getUnsent().size()>0){
            WorkManager.getInstance().cancelAllWork();
            WorkManager.getInstance().enqueue(new OneTimeWorkRequest.Builder(NetworkWorker.class).build());
        }
    }

    public void setListener(VotePresenterListener vpl){
        this.votePresenterListener = vpl;
    }

    public void removeListener(){
        this.votePresenterListener = null;
    }

    public void vote(Integer i){
        Vote vote = new Vote();
        vote.setRate(i);
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        vote.setDatetime(simpleDateFormat.format(new Date()));
        voteDao.insert(vote);
        WorkManager.getInstance().cancelAllWork();
        WorkManager.getInstance().enqueue(new OneTimeWorkRequest.Builder(NetworkWorker.class).build());
        votePresenterListener.onVoteDone(VotePresenterListener.VOTE_DONE_OK, "");
    }


    public interface VotePresenterListener{
        int VOTE_DONE_OK = 0;
        int VOTE_DONE_ERROR = 1;
        void onVoteDone(Integer resultCode, String message);
    }
}
