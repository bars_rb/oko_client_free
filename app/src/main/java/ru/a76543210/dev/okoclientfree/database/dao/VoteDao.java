package ru.a76543210.dev.okoclientfree.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.a76543210.dev.okoclientfree.model.Vote;

@Dao
public interface VoteDao {

    @Query("SELECT * FROM vote ORDER BY datetime")
    List<Vote> getAll();

    @Query("SELECT * FROM vote WHERE id = :id")
    Vote getById(int id);

    @Query("SELECT * FROM vote WHERE datetime>= :dateFrom AND datetime<=:dateTo ORDER BY datetime")
    List<Vote> getVotesByDate(String dateFrom, String dateTo);

    @Query("SELECT * FROM vote WHERE sent = 0")
    List<Vote> getUnsent();

    @Insert
    void insert(Vote vote);

    @Update
    void update(Vote vote);

    @Delete
    void delete(Vote vote);


}
