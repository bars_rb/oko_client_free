package ru.a76543210.dev.okoclientfree.presenter;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.model.SliceValue;
import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.database.DBProvider;
import ru.a76543210.dev.okoclientfree.database.dao.VoteDao;
import ru.a76543210.dev.okoclientfree.model.Vote;

public class VoteStatisticPresenter {
    private static VoteStatisticPresenter presenterInstance;
    private VoteDao voteDao;
    private Context context;

    private int[] colors = new int[]{
            R.color.vote_1,
            R.color.vote_2,
            R.color.vote_3,
            R.color.vote_4,
            R.color.vote_5,
    };

    public static synchronized VoteStatisticPresenter getInstance(Context context){
        if (presenterInstance == null){
            presenterInstance = new VoteStatisticPresenter(context);
        }
        return presenterInstance;
    }

    private VoteStatisticPresenter(Context context){
        this.context = context;
        voteDao = DBProvider.getAppDatabase(context).voteDao();
    }

    public List<Vote> getAllVotes(){
        return voteDao.getAll();
    }

    public List<Vote> getVotesByDate(String dateFrom, String dateTo){
        return voteDao.getVotesByDate(convertDateFromHumanToSQL(dateFrom), convertDateFromHumanToSQL(dateTo) + " 24");
    }

    public Map<Integer, Integer> getVotesStatistic(List<Vote> votes){
        Map<Integer, Integer> statistic = new LinkedHashMap<>();
        for(Vote v : votes){
            if(!statistic.containsKey(v.getRate())){
                statistic.put(v.getRate(), 1);
            }
            else {
                Integer val = statistic.get(v.getRate());
                val += 1;
                statistic.put(v.getRate(), val);
            }
        }
        return statistic;
    }

    public List<SliceValue> getVotesStatisticValues(List<Vote> votes){
        List<SliceValue> pieData = new LinkedList<>();

        for(Map.Entry<Integer, Integer> entry:getVotesStatistic(votes).entrySet()){
            pieData.add(
                       new SliceValue(entry.getValue(), ContextCompat.getColor(context, colors[entry.getKey()-1]))
                            .setLabel(entry.getKey().toString()+": "+entry.getValue())
                       );
        }
        return pieData;
    }

    public Map<String, Map<Integer, Integer>> getVotesDayByDayStatistic(List<Vote> votes){
        Map<String, Map<Integer, Integer>> dbdStatistic  = new LinkedHashMap<>();

        String currentDate;
        Map<Integer, Integer> currentVoteStatistic;
        Integer currentVotesCount;
        for(Vote v : votes){
            currentDate = v.getDatetime().substring(0,10);
            if(!dbdStatistic.containsKey(currentDate)){
                dbdStatistic.put(currentDate, new HashMap<Integer, Integer>());
            }
            currentVoteStatistic = dbdStatistic.get(currentDate);
            if(!currentVoteStatistic.containsKey(v.getRate())){
                currentVoteStatistic.put(v.getRate(), 0);
            }
            currentVotesCount = currentVoteStatistic.get(v.getRate());
            currentVotesCount++;
            currentVoteStatistic.put(v.getRate(), currentVotesCount);
        }
        return dbdStatistic;
    }

    private String convertDateFromHumanToSQL(String date){
        return date.substring(6,10)+"-"+date.substring(3,5)+"-"+date.substring(0,2);
    }

}
