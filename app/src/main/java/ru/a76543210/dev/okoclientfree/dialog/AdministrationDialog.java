package ru.a76543210.dev.okoclientfree.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fxn769.Numpad;
import com.fxn769.TextGetListner;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.activity.SettingsActivity;
import ru.a76543210.dev.okoclientfree.activity.StatisticActivity;

import static android.content.Context.MODE_PRIVATE;

public class AdministrationDialog extends DialogFragment {
    @BindView(R.id.num) Numpad numpad;
    @BindView(R.id.administration_menu) LinearLayout administration_menu;
    @BindView(R.id.entered_pin) TextView entered_pin;
    @BindView(R.id.pin_layout) LinearLayout pin_layout;
    @BindView(R.id.settings_layout) LinearLayout setting_layout;
    @BindView(R.id.statistic_layout) LinearLayout statistic_layout;


    SharedPreferences sharedPreferences;
    String PIN;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_administration, null, false);
        ButterKnife.bind(this, view);
        sharedPreferences = getActivity().getSharedPreferences("Settings", MODE_PRIVATE);
        PIN = sharedPreferences.getString("PIN", "0000");

        builder.setTitle(R.string.administration_menu);

        numpad.setOnTextChangeListner(new TextGetListner() {
            @Override
            public void onTextChange(String text, int digits_remaining) {

                if(text.length()==0)
                    entered_pin.setText(" ");
                    else entered_pin.setText(text);

                if(text.equals(PIN)){
                    pin_layout.setVisibility(View.GONE);
                    administration_menu.setVisibility(View.VISIBLE);
                }
            }
        });

        setting_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdministrationDialog.this.getDialog().cancel();
                Intent intent = new Intent(getContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        statistic_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdministrationDialog.this.getDialog().cancel();
                Intent intent = new Intent(getContext(), StatisticActivity.class);
                startActivity(intent);
            }
        });


        builder.setView(view)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AdministrationDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

}
