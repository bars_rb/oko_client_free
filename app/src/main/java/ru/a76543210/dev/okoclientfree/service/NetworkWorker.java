package ru.a76543210.dev.okoclientfree.service;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import ru.a76543210.dev.okoclientfree.database.AppDatabase;
import ru.a76543210.dev.okoclientfree.database.DBProvider;
import ru.a76543210.dev.okoclientfree.database.dao.VoteDao;
import ru.a76543210.dev.okoclientfree.model.Vote;

import static android.content.Context.MODE_PRIVATE;

public class NetworkWorker extends Worker {
    private static final String TAG = "NetworkWrk";

    private static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");
    private String deviceID;

    public NetworkWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.d(TAG, "Work started at " + new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()));

        deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        if(serviceTask()){
            Log.d(TAG, "Work finished at " + new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()));
            return Result.success();
        }
        else{
            Log.d(TAG, "Work failed at " + new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()));
            return Result.retry();
        }
    }

    private boolean serviceTask() {
        boolean result = true;

        NetworkService ns = new NetworkService(getApplicationContext());
        WebAPIService service = ns.webService();

        SharedPreferences sPref = getApplicationContext().getSharedPreferences("gps_pref", MODE_PRIVATE);

        VoteDao voteDao = DBProvider.getAppDatabase(getApplicationContext()).voteDao();

        List<Vote> unsentVotes = voteDao.getUnsent();
        if (unsentVotes.size() > 0) {
            Log.d(TAG, "Unsent votes exists. Send it at " + new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()));
            for (Vote vote : unsentVotes) {
                try {
                    Map<String, String> parameter = new HashMap<>();
                    parameter.put("hardware_id", deviceID);
                    parameter.put("rate", vote.getRate().toString());
                    parameter.put("datetime", vote.getDatetime());

                    Response<ResponseBody> response = service.sendVote(parameter).execute();
                    if (response.isSuccessful()) {
                        vote.setSent(true);
                        voteDao.update(vote);
                    } else {
                        Log.d(TAG, "Request error: "+parseErrorMessage(response.errorBody()));
                        result = false;
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result = false;
                    Log.d(TAG, "Network Error");
                    break;
                }
            }
        }
        return result;
    }

    private String parseErrorMessage(ResponseBody errorString) {
        JSONObject jsonObject = null;
        String message = null;
        try {
            jsonObject = new JSONObject(errorString.string());
            message = jsonObject.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }
}
