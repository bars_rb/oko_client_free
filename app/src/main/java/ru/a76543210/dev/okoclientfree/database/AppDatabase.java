package ru.a76543210.dev.okoclientfree.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;

import ru.a76543210.dev.okoclientfree.database.dao.VoteDao;
import ru.a76543210.dev.okoclientfree.model.Vote;

@Database(entities = {Vote.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract VoteDao voteDao();

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(final SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Vote ADD COLUMN sent INTEGER DEFAULT 0 NOT NULL");
        }
    };
}
