package ru.a76543210.dev.okoclientfree.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

public class NetworkService {
    private Context context;
    private WebAPIService webAPIService;

    NetworkService getModule() {
        return this;
    }

    public NetworkService(Context context) {
        this.context = context;
        SharedPreferences sPref = context.getSharedPreferences("Settings", MODE_PRIVATE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        String GPS_API_URL = sPref.getString("GPS_API_URL", "http://127.0.0.1");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GPS_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        webAPIService = retrofit.create(WebAPIService.class);
    }

    public WebAPIService webService() {
        return webAPIService;
    }

    public void changeBaseUrl(String newUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(newUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webAPIService = retrofit.create(WebAPIService.class);
    }

}

