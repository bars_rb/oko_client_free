package ru.a76543210.dev.okoclientfree.fragment;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.TreeMap;

public class FragmentFactory {
    public static ArrayList<String> FRAGMENTS = new ArrayList<>();
    static{
        FRAGMENTS.add("Two buttons");
        FRAGMENTS.add("Three buttons");
        FRAGMENTS.add("Five buttons");
    }

    public static Fragment getFragmentByName(String fragmentName){
        switch (fragmentName){
            case "Three buttons":return new ThreeButtonsFragment();
            case "Five buttons":return new FiveButtonsFragment();
            default: return new TwoButtonsFragment();
        }
    }
}
