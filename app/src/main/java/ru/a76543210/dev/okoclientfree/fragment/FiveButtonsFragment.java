package ru.a76543210.dev.okoclientfree.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.dialog.ThanksDialog;
import ru.a76543210.dev.okoclientfree.presenter.VotePresenter;

public class FiveButtonsFragment extends Fragment implements VotePresenter.VotePresenterListener {
    public final static String FRAGMENT_TAG = "FIVE_BUTTONS_FRAGMENT";
    @BindView(R.id.vote_1) ImageView vote_1;
    @BindView(R.id.vote_2) ImageView vote_2;
    @BindView(R.id.vote_3) ImageView vote_3;
    @BindView(R.id.vote_4) ImageView vote_4;
    @BindView(R.id.vote_5) ImageView vote_5;
    VotePresenter votePresenter;
    private ViewGroup container;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_five_buttons, container, false);
        this.container = container;
        ButterKnife.bind(this, v);

        votePresenter = VotePresenter.getInstance(getContext());
        votePresenter.setListener(this);

        vote_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                votePresenter.vote(1);
            }
        });

        vote_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                votePresenter.vote(2);
            }
        });

        vote_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { votePresenter.vote(3); }
        });

        vote_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                votePresenter.vote(4);
            }
        });

        vote_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                votePresenter.vote(5);
            }
        });
        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        votePresenter.removeListener();
    }

    @Override
    public void onVoteDone(Integer resultCode, String message) {
        if (resultCode==VOTE_DONE_OK) {
            ThanksDialog thanksDialog = new ThanksDialog();
            thanksDialog.setCancelable(false);
            thanksDialog.show(getFragmentManager(), "ThanksDialog");
        }

        if (resultCode==VOTE_DONE_ERROR) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }
}
