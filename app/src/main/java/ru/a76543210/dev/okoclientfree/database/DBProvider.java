package ru.a76543210.dev.okoclientfree.database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DBProvider {
    private static AppDatabase appDatabase;
    public static AppDatabase getAppDatabase(Context context){
        if(appDatabase==null)
            appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "votes")
                    .addMigrations(AppDatabase.MIGRATION_1_2)
                    .allowMainThreadQueries()
                    .build();
        return appDatabase;
    }
}
