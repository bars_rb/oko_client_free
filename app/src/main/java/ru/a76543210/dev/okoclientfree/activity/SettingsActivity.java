package ru.a76543210.dev.okoclientfree.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.dialog.AuthCredentialDialog;
import ru.a76543210.dev.okoclientfree.fragment.FragmentFactory;
import ru.a76543210.dev.okoclientfree.service.NetworkService;
import ru.a76543210.dev.okoclientfree.service.WebAPIService;

public class SettingsActivity extends AppCompatActivity implements AuthCredentialDialog.AuthCredentialDialogListener {
    @BindView(R.id.pin_edit_text) EditText pin_edit_text;
    @BindView(R.id.vote_view_spinner) Spinner vote_view_spinner;
    @BindView(R.id.save_settings) ImageView save_settings;
    @BindView(R.id.et_server_url) EditText et_server_url;

    SharedPreferences sharedPreferences;
    WebAPIService webAPIService;
    String deviceID;
    String GPS_API_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        final NetworkService ns = new NetworkService(getApplicationContext());
        webAPIService = ns.webService();

        deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        sharedPreferences = SettingsActivity.this.getSharedPreferences("Settings", MODE_PRIVATE);
        GPS_API_URL = sharedPreferences.getString("GPS_API_URL", "http://127.0.0.1");
        et_server_url.setText(GPS_API_URL);

        fillVoteViewSpinner();

        save_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = pin_edit_text.getText().toString();
                String selectedView = vote_view_spinner.getSelectedItem().toString();
                String uri = et_server_url.getText().toString();
                if (!uri.endsWith("/")) {
                    uri = uri + "/";
                }

                SharedPreferences.Editor editor = sharedPreferences.edit();

                if (!uri.equals(GPS_API_URL)) {
                    GPS_API_URL = uri;
                    editor.putString("GPS_API_URL", uri);
                    ns.changeBaseUrl(uri);
                    webAPIService = ns.webService();
                    checkDeviceRegistered();
                }

                if(changePinCorrect(pin)) {
                    if(pinChanged(pin)) {
                        editor.putString("PIN", pin);
                    }
                    editor.putString("VOTE_VIEW", selectedView);
                }
                else{
                    showPinWarningDialog();
                }
                editor.apply();
            }
        });


    }

    private void checkDeviceRegistered() {
        webAPIService.getStatus(deviceID).enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                if (response.code() == 200) {
                    Map<String, String> responseBody = response.body();
                    if (responseBody != null && "ok".equals(responseBody.get("status"))) {
                        Toast.makeText(getApplicationContext(), "Устройство зарегистрировано", Toast.LENGTH_SHORT).show();
                    }
                    else if ("not_found".equals(responseBody.get("status"))) {
                        signInDevice();
                    }
                } else {
                    String response_message = null;
                    if (response.errorBody() != null) {
                        response_message = parseErrorMessage(response.errorBody());
                    }
                    if (response.code() == 403 && "not_found".equals(response_message)) {
                        signInDevice();
                    }
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ошибка при проверке регистрации устройства.", Toast.LENGTH_SHORT).show();
                Log.e("Settings", "Register check error: " + t);
            }
        });
    }

    private void showPinWarningDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Warning!");
        alertDialog.setMessage("You must enter correct pin!\n" +
                "Pin length must bee 4 digits!");


        alertDialog.show();
    }

    private boolean pinChanged(String pin) {
        return (!sharedPreferences.getString("PIN", "-").equals(pin)&&pin.length()!=0);
    }

    private boolean changePinCorrect(String pin) {
        if(pin.length()==4) return true;
        if(pin.length()!=0) return false;
        if(sharedPreferences.contains("PIN")) return true;
        return false;
    }

    private void fillVoteViewSpinner() {
        String currentView = sharedPreferences.getString("VOTE_VIEW", FragmentFactory.FRAGMENTS.get(0));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, FragmentFactory.FRAGMENTS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vote_view_spinner.setAdapter(adapter);
        vote_view_spinner.setSelection(FragmentFactory.FRAGMENTS.indexOf(currentView));
    }


    private void signInDevice() {
        AuthCredentialDialog newExerciseDialog = new AuthCredentialDialog();
        newExerciseDialog.setDialogListener(this);
        newExerciseDialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }


    private String parseErrorMessage(ResponseBody errorString) {
        JSONObject jsonObject;
        String message;
        try {
            jsonObject = new JSONObject(errorString.string());
            message = jsonObject.getString("message");
        } catch (JSONException e) {
            message = "Wrong response";
            e.printStackTrace();
        } catch (IOException e) {
            message = "IO exception";
            e.printStackTrace();
        }
        return message;
    }

    @Override
    public void onCredentialsEntered(String username, String password) {
        Map<String, String> parameter = new HashMap<>();
        parameter.put("hardware_id", deviceID);
        parameter.put("username", username);
        parameter.put("password", password);

        webAPIService.signIn(parameter).enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                if (response.code() == 200) {
                    Map<String, String> responseBody = response.body();
                    if (responseBody != null && "ok".equals(responseBody.get("status"))) {
                        Toast.makeText(getApplicationContext(), "Устройство зарегистрировано", Toast.LENGTH_SHORT).show();
                    }
                    else if ("error".equals(responseBody.get("status"))) {
                        Toast.makeText(getApplicationContext(), "Ошибка: "+responseBody.get("message"), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String response_message = null;
                    if (response.errorBody() != null) {
                        response_message = parseErrorMessage(response.errorBody());
                    }
                    Toast.makeText(getApplicationContext(), "Ошибка: "+response_message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ошибка при регистрации.", Toast.LENGTH_SHORT).show();
                Log.e("Settings", "Register check error: " + t);
            }
        });
    }
}
