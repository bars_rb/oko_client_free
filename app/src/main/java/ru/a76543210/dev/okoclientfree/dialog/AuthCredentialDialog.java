package ru.a76543210.dev.okoclientfree.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import ru.a76543210.dev.okoclientfree.R;
import ru.a76543210.dev.okoclientfree.activity.SettingsActivity;

public class AuthCredentialDialog extends DialogFragment {
    EditText et_username;
    EditText et_password;

    public interface AuthCredentialDialogListener {
        public void onCredentialsEntered(String username, String password);
    }

    private SettingsActivity dialogListener;

    public void setDialogListener(SettingsActivity dialogListener) {
        this.dialogListener = dialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Введите свои логин и пароль:");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.auth_credentials_dialog, null, false);
        et_username = view.findViewById(R.id.et_username);
        et_password = view.findViewById(R.id.et_password);


        builder.setView(view)
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialogListener.onCredentialsEntered(et_username.getText().toString(), et_password.getText().toString());
                    }
                });
        return builder.create();
    }
}

