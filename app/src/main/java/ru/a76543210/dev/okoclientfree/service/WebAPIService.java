package ru.a76543210.dev.okoclientfree.service;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WebAPIService {
    @POST("vote")
    Call<ResponseBody> sendVote(@Body Map<String, String> vote);

    @POST("device/sign_in")
    Call<Map<String, String>> signIn(@Body Map<String, String> parameters);

    @GET("device/status/{device_id}")
    Call<Map<String, String>> getStatus(@Path("device_id") String device_id);

}
