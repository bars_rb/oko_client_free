package ru.a76543210.dev.okoclientfree.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.a76543210.dev.okoclientfree.R;

public class DailyVotesResultsAdapter extends RecyclerView.Adapter<DailyVotesResultsAdapter.DailyVotesResiltsViewHolder> {
    Map<String, Map<Integer, Integer>> dbdStatistic;
    LinkedList<String> resultDates;
    //private OnExerciseResultsClickListener onExerciseResultsClickListener;

    private Context context;

    class DailyVotesResiltsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        @BindView(R.id.date_text_view) TextView date_text_view;

        @BindView(R.id.vote_1_text_view) TextView vote_1_text_view;
        @BindView(R.id.vote_1_image_view) ImageView vote_1_image_view;
        @BindView(R.id.vote_2_text_view) TextView vote_2_text_view;
        @BindView(R.id.vote_2_image_view) ImageView vote_2_image_view;
        @BindView(R.id.vote_3_text_view) TextView vote_3_text_view;
        @BindView(R.id.vote_3_image_view) ImageView vote_3_image_view;
        @BindView(R.id.vote_4_text_view) TextView vote_4_text_view;
        @BindView(R.id.vote_4_image_view) ImageView vote_4_image_view;
        @BindView(R.id.vote_5_text_view) TextView vote_5_text_view;
        @BindView(R.id.vote_5_image_view) ImageView vote_5_image_view;


        DailyVotesResiltsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(onExerciseResultsClickListener!=null)
//                        onExerciseResultsClickListener.onExerciseResultsClick(exerciseList.get(getLayoutPosition()));
//                }
//            });
        }

        void bind(String date, Map<Integer, Integer> votes) {
            date_text_view.setText(date);
            for(Map.Entry<Integer, Integer> vote: votes.entrySet()){
                switch (vote.getKey()){
                    case 1:
                        vote_1_image_view.setVisibility(View.VISIBLE);
                        vote_1_text_view.setVisibility(View.VISIBLE);
                        vote_1_text_view.setText(vote.getValue().toString());
                        break;
                    case 2:
                        vote_2_image_view.setVisibility(View.VISIBLE);
                        vote_2_text_view.setVisibility(View.VISIBLE);
                        vote_2_text_view.setText(vote.getValue().toString());
                        break;
                    case 3:
                        vote_3_image_view.setVisibility(View.VISIBLE);
                        vote_3_text_view.setVisibility(View.VISIBLE);
                        vote_3_text_view.setText(vote.getValue().toString());
                        break;
                    case 4:
                        vote_4_image_view.setVisibility(View.VISIBLE);
                        vote_4_text_view.setVisibility(View.VISIBLE);
                        vote_4_text_view.setText(vote.getValue().toString());
                        break;
                    case 5:
                        vote_5_image_view.setVisibility(View.VISIBLE);
                        vote_5_text_view.setVisibility(View.VISIBLE);
                        vote_5_text_view.setText(vote.getValue().toString());
                        break;
                }
            }
//            exercise_results_flex_box.removeAllViews();
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            layoutParams.setMargins(8, 8, 8, 0);
//            for (ExerciseResultsViews resultsView : workoutResultsViews.get(exercise))
//                exercise_results_flex_box.addView(resultsView.getView(), layoutParams);
//        }
        }

//    public interface OnExerciseResultsClickListener{
//        void onExerciseResultsClick(Exercise exercise);
//    }

        public void refreshData() {
            resultDates.clear();
            resultDates.addAll(dbdStatistic.keySet());
            notifyDataSetChanged();
        }

    }


    public DailyVotesResultsAdapter( Map<String, Map<Integer, Integer>> myDataset, Context context) {
        //Map<String, Map<Integer, Integer>> dbdStatistic
        this.context = context;
        dbdStatistic = myDataset;
        resultDates = new LinkedList<>(dbdStatistic.keySet());
        //this.onExerciseResultsClickListener = onExerciseResultsClickListener;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public DailyVotesResultsAdapter.DailyVotesResiltsViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                                   int viewType) {
        // create a new view
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_daily_vote_result, parent, false);
        return new DailyVotesResiltsViewHolder(inflate);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull DailyVotesResultsAdapter.DailyVotesResiltsViewHolder holder, int position) {
        holder.bind(resultDates.get(position), dbdStatistic.get(resultDates.get(position)));
    }

    @Override
    public int getItemCount() {
        return dbdStatistic.size();
    }



}